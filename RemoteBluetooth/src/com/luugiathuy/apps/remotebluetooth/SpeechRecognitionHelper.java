package com.luugiathuy.apps.remotebluetooth;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Class helper for speech recognition
 */
public class SpeechRecognitionHelper extends Activity{
	
	private static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;
	private Button startSpeechRecogButton;
	private static Context context;
	public BluetoothCommandService mCommandService = null;
	
	/**
	 * Starts the process of recognition . Checks for Activiti for speech recognition .
	 * If Activiti not, sends the user to the market to install Voice Search
	 * Google. If the asset has to Detect , then sends Intent to run it .
	 *
	 * @param OwnerActivity Activiti , which initiated the process recognition
	 * 
	 */
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.speech_command);
        startSpeechRecogButton = (Button) findViewById(R.id.button1);
    }
	
	public static void setContext(Context ctx){
        context = ctx;
    }
	
	@Override
	protected void onResume() {
		super.onResume();
		SpeechRecognitionHelper.setContext(this);
	}
	
	@Override
	protected void onDestroy() {
		SpeechRecognitionHelper.setContext(null);
		super.onDestroy();
	}

	protected void onPause(){
		SpeechRecognitionHelper.setContext(null);
		super.onPause();
	}
	
	public void startSpeechRecognition(View view) {
		Activity speech_activity = (Activity) context;
		run(speech_activity);
	}
	
	public static void run(Activity ownerActivity) {
		// check whether there is a recognition for Activity
		if (isSpeechRecognitionActivityPresented(ownerActivity) == true) {
			// if there is - run recognition
			startRecognitionActivity(ownerActivity);
		} else {
			// begin the installation process
			installGoogleVoiceSearch(ownerActivity);
		}			
	}
	
	/** 
	 * Checks for Activity able to perform speech recognition
	 * 
	 * @param ownerActivity Activiti that requested verification
	 * @return true - if there is , false - if there is no such Activity 
	 */
	private static boolean isSpeechRecognitionActivityPresented(Activity ownerActivity) {
		try {
			// obtain a copy of the package manager
			PackageManager pm = ownerActivity.getPackageManager();
			// get a list of Activiti able to process the request for recognition
			List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
			
			if (activities.size() != 0) {	//if the list is not empty	
				return true;				// is able to recognize speech
			}
		} catch (Exception e) {
			
		}
		
		return false; // do not know how it Edge
	}
	
	/**
	 * Otpavlyaet Intent with a request for speech recognition
	 * @param ownerActivity iniirovavshaya request Activiti
	 */
	private static void startRecognitionActivity(Activity ownerActivity) {
		
		// create an Intent with the action RecognizerIntent.ACTION_RECOGNIZE_SPEECH
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		
		//function adds additional parameters :
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Voice Search Information");	// text prompts the user to
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);	// recognition model
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);	// amount of an outcome that we want to get
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ru-RU");
        
        // start the Activiti and expect it to result
        ownerActivity.startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
	        List<String> results = data.getStringArrayListExtra(
	                RecognizerIntent.EXTRA_RESULTS);
	        String spokenText = results.get(0);
	        Toast.makeText(getApplicationContext(), "Successful:" + results.get(0), Toast.LENGTH_SHORT).show();
	        // Do something with spokenText.
	    } else  {
        	Toast.makeText(getApplicationContext(), "Speech Recognized Failure", Toast.LENGTH_SHORT).show();
        }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * Requests permission to posing the voice search Google, displaying the dialog. If the resolution
	 * poluchino - guides the user in the market .
	 * @param ownerActivity Activiti iniirovavshaya installation
	 */
	private static void installGoogleVoiceSearch(final Activity ownerActivity) {
		
		// create a dialog that asks the user if he wants to
		// install Voice Search
		Dialog dialog = new AlertDialog.Builder(ownerActivity)
			.setMessage("For speech recognition , you must install Voice Search Google")	// post
			.setTitle("attention")	// dialog title
			.setPositiveButton("Set", new DialogInterface.OnClickListener() {	// positive button

				// handler clicking on the Install button
				@Override
				public void onClick(DialogInterface dialog, int which) {	
					try {
						// create an Intent to open a page with the Market app
						// Voice search package name : com.google.android.voicesearch
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.voicesearch"));
						// configure flags to market not to Pope in the history of our application (stack Activiti )
						intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
						//send Intent
						ownerActivity.startActivity(intent);
					 } catch (Exception ex) {
						 // Failed to open market
						// For example because it is not set
						// Nothing podalaesh
					 }					
				}})
				
			.setNegativeButton("Cancel", null)	// negative button
			.create();
		
		dialog.show();	// show dialogue		 
	}	
}
